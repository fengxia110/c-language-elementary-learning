#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
	int arr1[3] = { 1,2,3 };
	int arr2[3] = { 4,5,6 };
	int cz = sizeof(arr1) / sizeof(arr1[0]);
	int tmp = 0;
	int i = 0;
	
	for (i = 0; i < cz; i++)
	{
		tmp = arr1[i];
		arr1[i] = arr2[i];
		arr2[i] = tmp;
	}
	
	return 0;
}