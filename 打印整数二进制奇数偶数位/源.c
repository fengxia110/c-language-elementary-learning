#define  _CRT_SECURE_NO_WARNINGS 1
//第一位为奇位；
#include<stdio.h>
int main()
{
	int n = 10;
	for (int i = 1; i <= 32; i++)
	{
		if(i==2)
			printf("偶数位为： ");
		if (i % 2 == 0)
		{
			if (((n >> (i-1)) & 1 )== 1)
				printf("1 ");
			else
				printf("0 ");
		}
	}
	printf("\n");
	for (int i = 1; i <= 32; i++)
	{
		if (i == 1)
			printf("奇数位为： ");
		if (i % 2 != 0)
		{
			if (((n >> (i - 1)) & 1) == 1)
				printf("1 ");
			else
				printf("0 ");
		}
	}
    printf("\n");
	return 0;
}