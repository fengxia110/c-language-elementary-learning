#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int count(int n)
//{
//	int arr[50]={0,1,1};
//  for(int i=0; i<=n; i++)
//  {
//      arr[i]=arr[i-1]+arr[i-2];
//  }
//  return arr[n];
// 
//}
int count(int n)
{
	if (n == 1)
		return 1;
	else if (n == 2)
		return 1;
	else
		return count(n - 1) + count(n - 2);
}

int main()
{
	int n = 0;
	int ret = 0;
	scanf("%d", &n);
	ret=count(n);
	printf("%d\n", ret);
	return 0;
}