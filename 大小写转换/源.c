#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<ctype.h>

int main()
{
    char ch = 0;
    while (scanf("%c", &ch) != EOF)
    {
        getchar();
        /*if (ch >= 65 && ch <= 90)
            printf("%c\n", ch + 32);
        else
            printf("%c\n", ch - 32);*/
        if (isupper(ch))
            printf("%c ", tolower(ch));
        else if (islower(ch))
            printf("%c ", toupper(ch));
    }
    return 0;
}