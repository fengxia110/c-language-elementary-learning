#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int compute(int n, int k)
{
	if (k == 0)
		return 1;
	if (k == 1)
		return n;
	else
		return n * compute(n, k - 1);
}

int main()
{
	int n = 0;
	int k = 0;
	int ret = 0;
	scanf("%d %d", &n, &k);
	ret = compute(n, k);
	printf("%d\n", ret);
	return 0;
}