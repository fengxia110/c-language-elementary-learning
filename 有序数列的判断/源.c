#define  _CRT_SECURE_NO_WARNINGS 1
//一个整数序列，判断是否是有序，指序列中的整数从小到大排序或者从大到小排序(相同元素也视为有序)
#include<stdio.h>
int main()
{
    int n = 0;
    int arr[50] = { 0 };
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
        scanf("%d ", &arr[i]);
    int flag1 = 0;//升序
    int flag2 = 0;//降序
    for (int i = 0; i < n - 1; i++)
    {
        if (arr[i] > arr[i + 1])
            flag2 = 1;
        else
            flag1 = 1;
    }
    if (flag1 + flag2 <= 1)
        printf("sorted\n");
    else
        printf("unsorted\n");
    return 0;
}