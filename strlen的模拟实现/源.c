#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int count(char* arr)
//{
//	int ret = 0;
//	while (*(arr++))
//		ret++;
//	return ret;
//}

int count(char* arr)
{
	if ('\0' == *arr)
		return 0;
	else
		return 1 + count(1 + arr);


}

int main()
{
	char arr[50] = { "0" };
	int ret = 0;
	scanf("%s", arr);
	ret = count(arr);
	printf("%d\n", ret);
	return 0;
}