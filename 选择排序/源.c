#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	int arr[10] = { 1, 3, 5, 7, 4, 6, 2, 8, 9, 0 };
	int n = sizeof(arr) / sizeof(arr[0]);
	int i = 0;
	int j = 0;
	int sign = 0;
	int tmp = 0;
	for (i = 0; i < n - 1; i++)
	{
		//找出最小值的坐标
		sign = i;
		for (j = i; j < n; j++)
		{
			if (arr[sign] > arr[j])
				sign = j;
		}
		//交换他们
		tmp = arr[i];
		arr[i] = arr[sign];
		arr[sign] = tmp;
	}
	for (i = 0; i < n; i++)
		printf("%d ", arr[i]);
	return 0;
}