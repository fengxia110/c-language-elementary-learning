#define  _CRT_SECURE_NO_WARNINGS 1
//给定一个长度为 n 的非降序数组和一个非负数整数 k ，
//要求统计 k 在数组中出现的次数
#include<stdio.h>

int main()
{
	int arr[10] = { 1,1,1,5,5,7,7,33,33,44 };
	int k = 0;
	int quantity = 0;
	scanf("%d", &k);
	//判断数列是否为空
	if (sizeof(arr) == 0)
		goto this;
	int left = 0;
	int right = sizeof(arr) / sizeof(arr[0])-1;
	int mid = 0;
	//寻找第一次K出现的地方
	while (left < right)
	{
		mid = (left + right) / 2;
		if (arr[mid] < k)//K在右边
			left = mid + 1;
		else //K在左边,或K=mid
			right = mid ;
		
	}
	//判断有几个k
	for ( ; arr[left] == k;)
	{
		quantity++;
		left++;
	}
this:
	printf("%d\n", quantity);

}