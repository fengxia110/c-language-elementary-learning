#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//输入一个整数(1≤N≤50)。
//
//第二行输入N个整数（大于1小于50）.
//
//第三行输入想要进行删除的一个整数。
//输出删除指定数字之后的序列。

//***程序中额外加有保护机制
int main()
{
    int n = 0;
    int arr[50] = { 0 };
    int newarr[50] = { 0 };
    int x = 0;
    do
    {
        scanf("%d", &n);
        if (n >= 0 && n <= 50)
            break;
        else
            printf("输入错误请重新输入\n");
    } while (1);
    for (int i = 0; i < n; )
    {
        scanf("%d", &arr[i]);
        if (arr[i] >= 1 && arr[i] <= 50)
            i++;
        else
            printf("输入错误请重新输入\n");
    }
    scanf("%d", &x);
    for (int i = 0, j = 0; i < n && j < n - 1; i++)
    {
      
         if (arr[i] != x)
         {
             newarr[j] = arr[i];
             printf("%d ", newarr[j]);
             j++;
         }
    }
    return 0;
}