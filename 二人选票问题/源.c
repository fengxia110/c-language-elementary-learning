#define  _CRT_SECURE_NO_WARNINGS 1

//有两名候选人分别是A和B，每个人必须并且只能投一票，最终得票多的人当选.

#include<stdio.h>

int main()
{
    int a = 0;
    int b = 0;
    char c;
    do
    {
        scanf("%c", &c);
        if (c == '0')
            break;
        else if (c == 'A')
            a++;
        else
            b++;
    } while (c);
    if (a > b)
        printf("A\n");
    else if (b > a)
        printf("B\n");
    else
        printf("E\n");
    return 0;
}