#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int digitsum(int n)
{
	if (n >= 0 && n <= 9)
		return n;
	else
		return n%10 + digitsum(n / 10);
}

int main()
{
	int n = 0;
	int sum = 0;
	scanf("%d", &n);
	sum = digitsum(n);
	printf("%d\n", sum);
	return 0;
}