#define  _CRT_SECURE_NO_WARNINGS 1
//最大公约数，最小公倍数之和
//公约数用a%b=c,这种方法使用循环来求，当C=0时，b为最大公约数
//最小公倍数则为a*b/最大公约数
#include <stdio.h>
int main() {
    long long a, b, m, n, c;
    scanf("%lld %lld", &a, &b);
    m = a;
    n = b;
    while (n) {
        c = m % n;
        m = n;
        n = c;
    }
    printf("%lld\n", a*b/m+m);
}