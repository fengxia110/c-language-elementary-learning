#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//也可以用数组的方法进行实现
//算出的每位数依次存放，再打印

void count(unsigned int a)
{
    unsigned int b = 0;
    if (a < 6)
        printf("%d", a);
    else
    {
        b = a % 6;
        count(a / 6);
        printf("%d", b);
    }
}

int main()
{
    unsigned int a = 0;
    scanf("%u ", &a);
    count(a);
    return 0;
}