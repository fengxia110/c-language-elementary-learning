#define  _CRT_SECURE_NO_WARNINGS 1
//输入n个成绩，换行输出n个成绩中最高分数和最低分数的差。
#include<stdio.h>
int main()
{
    int n = 0;
    scanf("%d", &n);
    int arr[10000] = { 0 };
    int max = 0;
    int min = 100;
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
        if (max < arr[i])
            max = arr[i];
        if (min > arr[i])
            min = arr[i];
    }
    printf("%d\n ", max - min);
    return 0;
}