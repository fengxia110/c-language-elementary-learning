#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
	int n = 0;
	int sum = 0;
	int ret = 0;
	for (n = 100; n < 1000; n++)
	{
		ret = n;
		sum = 0;
		for (int i = 0; i < 3; i++)
		{
			sum += (ret % 10) * (ret % 10) * (ret % 10);
			ret = ret / 10;
		}
		if (sum == n)
			printf("%d ", n);
	}
}