#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//第一行包含一个整数n，表示一个方阵包含n行n列，用空格分隔。(1≤n≤10)
//从2到n + 1行，每行输入n个整数，共输入n * n个数。
//如果输入方阵是上三角矩阵输出"YES"并换行，否则输出"NO"并换行。
int main()
{
    int n = 0;
    int arr[10][10] = { 0 };
    scanf("%d ", &n);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            scanf("%d ", &arr[i][j]);
    }
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j <= i - 1; j++)
            if (arr[i][j] != 0)
            {
                printf("NO\n");
                return 0;
            }
    }
    printf("YES\n");
    return 0;
}