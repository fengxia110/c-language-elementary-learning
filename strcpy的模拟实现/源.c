#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include <assert.h>
char * my_strcpy(char* dest, const char* src)
{
	char* begin = dest;
	assert(dest&&src);
	while (*dest++ = *src++);
	return begin;
}

int main()
{
	char ch1[] = "abcdefg";
	char ch2[100];
	
	printf("%s\n", my_strcpy(ch2, ch1));
	return 0;
}