#define  _CRT_SECURE_NO_WARNINGS 1
//第一行包含一个正整数n，表示总共有N个数。
//每行一个正整数k，为序列中每一个元素的值。(1 ≤ n ≤ 105，1 ≤ k ≤ n)
//输出：从小到大输出，去除重复的数字。
#include<stdio.h>

int main()
{
    int n = 0;
    int arr[100000] = { 0 };
    int k = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &k);
        arr[k]++;
    }
    for (int i = 0; i < n; i++)
    {
        if (arr[i] != 0)
            printf("%d ", i);
    }
    return 0;
}