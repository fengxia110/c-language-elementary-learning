#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
    int arr[3] = { 1,2,3 };
    int* pa = arr;
    int cz = sizeof(arr) / sizeof(arr[0]);
    for (int i = 0; i < cz; i++)
    {
        printf("%d ", *(pa + i));
    }
    return 0;
}
