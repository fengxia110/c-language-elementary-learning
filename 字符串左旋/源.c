﻿#define _CRT_SECURE_NO_WARNINGS 1

//实现一个函数，可以左旋字符串中的k个字符。
//例如：
//ABCD左旋一个字符得到BCDA
//ABCD左旋两个字符得到CDAB

#include <stdio.h>

////字符串左旋，又利用了另外的字符串来存放需要旋转的字符
//void left_handed(char* ch, int n)
//{
//    char left_ch[10] = { 0 };
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < n; i++)
//        left_ch[i] = *(ch + i);
//    i = n;
//    j = 0;
//    while (*(ch + i) != '\0')
//    {
//        *(ch + j) = *(ch + i);
//        i++;
//        j++;
//    }
//    i = 0;
//    while (left_ch[i] != '\0')
//    {
//        *(ch + j) = left_ch[i];
//        i++;
//        j++;
//    }
//}
//int main()
//{
//    char ch[10] = "abcdswe";
//    int n = 0;
//    scanf("%d", &n);
//    left_handed(ch, n);
//    printf("%s ", ch);
//    return 0;
//}
#include<string.h>
//字符串左旋，依次左旋一个

//void left_handed(char* arr, int sz)
//{
//	char tmp = *arr;
//	strncpy(arr, arr + 1, sz - 1);
//	*(arr + sz - 1) = tmp;
//}
//
//int main()
//{
//	char arr[10] = "abcdefgh";
//	int n = 0;
//	int sz = strlen(arr);
//	scanf("%d", &n);
//	for (int i = 0; i < n%sz; i++)
//	{
//		left_handed(arr, sz);
//	}
//	printf("%s\n", arr);
//	return 0;
//}

//翻转字母法
//abcdefgh
//bahgfedc
//cdefghab

void roll(char* arr1, char* arr2)
{
	char tmp;
	while (arr1 < arr2)
	{
		tmp = *arr1;
		*arr1 = *arr2;
		*arr2 = tmp;
		arr1++;
		arr2--;
	}
}

int main()
{
	char arr[10] = "abcdefgh";
	int n = 0;
	int sz = strlen(arr);
	scanf("%d", &n);
	roll(arr, arr + n%sz - 1);
	roll(arr + n, arr + sz - 1);
	roll(arr, arr + sz - 1);
	printf("%s\n", arr);
	return 0;
}