﻿#define  _CRT_SECURE_NO_WARNINGS 1

//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，
//请编写程序在这样的矩阵中查找某个数字是否存在。
#include<stdio.h>

////遍历法找数字
//int main()
//{
//      int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//      int n = 3;
//      int m = 3;
//    
//    //输出所有数组
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//            printf("%3d ", arr[i][j]);
//        printf("\n");
//    }
//    //遍历法
//    int find = 0;
//    int k = 0;
//    scanf("%d", &find);
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            if (find == arr[i][j])
//            {
//                printf("找到啦在%d行%d列是%d\n", i + 1, j + 1, arr[i][j]);
//                k = 1;
//            }
//        }
//    }
//    if (k)
//        ;
//    else
//        printf("没找到。\n");
//    return 0;
//}


//从右上角开始找数字
int main()
{
    int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
    int n = 3;
    int m = 3;
    //输出所有数组
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            printf("%3d ", arr[i][j]);
        printf("\n");
    }
    //开始查找
    int find = 0;
    int i = 0;
    int j = m - 1;
    scanf("%d", &find);
    while (i < n && j >= 0)
    {
        if (arr[i][j] < find)
        {
            i++;
        }
        else if (arr[i][j] > find)
        {
            j--;
        }
        else
        {
            printf("%d行%d列\n", i, j);
            return 0;
        }
    }
    printf("找不到!\n");
    return 0;
}