#define  _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

//游戏本体
void game()
{
	//创建两个二维数组
	char mine[ROWS][COLS] = { 0 };
	char show[ROWS][COLS] = { 0 };

	

	init_board(mine, ROWS, COLS, '0');
	init_board(show, ROWS, COLS, '*');

	//设置雷
    set_mine(mine, ROW, COL, COUNT);
	//打印
	display_board(show, ROW, COL);
	//发现雷
	find_mine(mine, show, ROW, COL, COUNT);
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请输入：\n");
		scanf("%d", &input);
		switch (input)
		{
		case 0:
			printf("退出游戏！\n");
			break;
		case 1:
			printf("开始游戏！\n");
			game();
			break;
		default:
			printf("输入错误请重新输入！\n");
			break;
		}
	}while (input);

	return 0;
}