#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define ROW 9
#define COL 9
#define ROWS ROW+2
#define COLS COL+2
#define COUNT 10

void menu();
void game();
void init_board(char arr[ROWS][COLS], int rows, int cols, char set);
void set_mine(char arr[ROWS][COLS], int row, int col, int count);
void display_board(char arr[ROWS][COLS], int row, int col);
int find_mine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col, int count);