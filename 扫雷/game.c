#define  _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

//打印菜单
void menu()
{
	printf("                                  \n");
	printf("              1 . 开始游戏        \n");
	printf("              0 . 退出游戏        \n");
	printf("                                  \n");
}


void init_board(char arr[ROWS][COLS], int rows, int cols, char set) {
	
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			arr[i][j] = set;
			
		}
	}
}

//设置雷
void set_mine(char arr[ROWS][COLS],int row, int col, int count)
{
	int x;
	int y;
	
	while (count > 0)
	{
		x = rand() % row;
	    y = rand() % col;
		if (arr[x + 1][y + 1] == '0')
		{
			arr[x + 1][y + 1] = '1';
			count--;
	
		}
	}
	//调试专用
	int i = 0;
	int j = 0;
	for (j = 0; j <= col; j++)
	{
		printf("%d ", j);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", arr[i][j]);
		}
		printf("\n");
	}
}

//打印
void display_board(char arr[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	for (j = 0; j <= col; j++)
	{
		printf("%d ", j);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", arr[i][j]);
		}
		printf("\n");
	}
}

//发现雷
int find_mine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col, int count)
{
	int x = 0;
	int y = 0;
	char ret = '0';
	for (; count > 0; )
	{
		printf("请输入坐标：\n");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y <= row && y < col)
		{
			if (show[x][y] == '*')
			{
				if (mine[x][y] == '1')
				{
					printf("你已经被炸死啦！\n");
					return 0;
				}
				else
				{
					ret = (mine[x - 1][y - 1] + mine[x - 1][y] + mine[x - 1][y + 1] + mine[x][y - 1] + mine[x][y + 1] + mine[x + 1][y - 1] + mine[x + 1][y] + mine[x + 1][y + 1])-7*'0';
					show[x][y] = ret;
					printf("%c ", show[x][y]);
					display_board(show, ROW, COL);
					count--;
				}
			}
			else
			{
				printf("坐标已被占用，请重新输入！\n");
			}
		}
	}
	printf("你已获胜！\n");
	return 0;
}
