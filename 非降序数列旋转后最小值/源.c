#define  _CRT_SECURE_NO_WARNINGS 1
//有一个长度为 n 的非降序数组，比如[1, 2, 3, 4, 5]，将它进行旋转，
//即把一个数组最开始的若干个元素搬到数组的末尾，变成一个旋转数组，
//比如变成了[3, 4, 5, 1, 2]，或者[4, 5, 1, 2, 3]这样的。
//请问，给定这样一个旋转数组，求数组中的最小值。
#include<stdio.h>
//找最小值
int find_min(const int* arr, int cz)
{
    int left = 0;
    int right = cz - 1;
    //非递减数列（未经旋转）
    if (arr[left] < arr[right])
        return arr[left];
    //（经过旋转）
    while (left < right)
    {
        int mid = (left + right) / 2;
        if (arr[mid] > arr[right])//最小值在右边
            left = mid + 1;
        else if (arr[mid] < arr[right])//最小值在左边
            right = mid;
        else
            right--;
    }
    return arr[left];

}

//旋转
void rotate(int* arr, int cz, int n)
{
	int arr1[10] = { 0 };
	int i = 0;
	//前几个元素的副本
	for (i = 0; i < n; i++)
	{
		arr1[i] = *(arr + i);
	}
	//后几个元素复制到前面
	for(i=0; i<cz-n; i++)
		*(arr + i) = *(arr + n + i);
	//前几个元素复制到后方
	for (i = 0; i < n; i++)
		*(arr + cz - n + i) = arr1[i];
}


int main()
{
	int arr[5] = { 1,2,3,4,5 };
	int cz = sizeof(arr) / sizeof(arr[0]);
	int n = 0;
	scanf("%d", &n);
	rotate(arr, cz, n);
    //输出旋转后的数列
	for (int i = 0; i < cz; i++)
		printf("%d ", arr[i]);
    printf("\n");
    //判断最小值
    int ret=find_min(arr, cz);
    printf("%d\n", ret);
    


}
