#define  _CRT_SECURE_NO_WARNINGS 1

//输入描述：
//多组输入，一个整数（2~20），表示直角三角形直角边的长度，即“ * ”的数量，也表示输出行数。
//
//输出描述：
//针对每行输入，输出用“ * ”组成的对应长度的直角三角形，每个“ * ”后面有一个空格。

#include <stdio.h>

int main()
{
    int n = 0;
    while (scanf("%d", &n) == 1)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (j >= n - i - 1)
                {
                    printf("*");
                    printf(" ");
                }
                else
                {
                    printf(" ");
                    printf(" ");
                }
            }
            printf("\n");
        }
    }
    return 0;
}