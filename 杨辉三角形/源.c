﻿#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    int n = 0;
    scanf("%d", &n);
    int arr[50][50] = { 0 };
    arr[0][0] = 1;
    int i = 0;
    int j = 0;
    //初始化部分数值
    for (i = 1; i < n; i++)
    {
        arr[i][0] = 1;
        arr[i][i] = 1;
    }
    //计算其他数值；
    for (i = 2; i < n; i++)
    {
        for (j = 1; j < i ; j++)
        {
            arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
        }
    }
    //输出
    for( i = 0; i < n; i++)
    {
        for ( j = 0; j <= i; j++)
        {
            printf("%2.d ", arr[i][j]);
        }
        printf("\n");
    }
    return 0;
}