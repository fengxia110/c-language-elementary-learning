#define  _CRT_SECURE_NO_WARNINGS 1

int NumberOf1(int n) 
{
    
    int count = 0;
    for (int i = 0; i < 32; i++)
    {
        if ((n >> i) & 1 == 1)
            count++;
    }
    return count;
}

int main()
{
    int n = 10;
    int count=NumberOf1(n);
    printf("%d\n", count);
    return 0;
}