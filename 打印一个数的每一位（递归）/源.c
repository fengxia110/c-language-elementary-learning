#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

void print(int n)
{
	if (n >= 0 && n <= 9)
		printf("%d ", n);
	else
	{
		printf("%d ", n % 10);
		print(n / 10);
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	print(n);
	return 0;
}