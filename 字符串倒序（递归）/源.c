#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//void reverse_string(char* arr, int sz)
//{
//	char tmp;
//	for (int i = 0; i < sz/2; i++)
//	{
//		tmp = *(arr+i);
//		*(arr + i) = *(arr + sz - 1 - i);
//		*(arr + sz - 1 - i) = tmp;
//	}
//}

void reverse_string(char* arr, int sz)
{
	char tmp;
	tmp = *(arr);
	if (sz >= 2)
	{
		*arr = *(arr + sz - 1);
		reverse_string(arr + 1, sz - 2);
		*(arr + sz - 1) = tmp;
	}
}

int main()
{
	char arr[50] = { "0" };
	scanf("%s", arr);
	int sz = strlen(arr);
	reverse_string(arr,sz);
	printf("%s\n", arr);
	return 0;
}