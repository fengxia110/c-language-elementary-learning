#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
int main()
{
	//使用输出函数，要引用相关的头文件stdio.h
	//输出内容要用双引号括住
	//C语言语法规定：每条语句结束要有英文的分号
	printf("hello world");

	//因为主函数的类型为int,我们这里要返回值，返回0即可
	return 0;
}