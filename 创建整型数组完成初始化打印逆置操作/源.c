#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。



void init(int arr[], int cz)
{
	int i = 0;
	for (i = 0; i < cz; i++)
	{
		arr[i] = 0;
	}
}


void print(int arr[], int cz)
{
	int i = 0;
	for (i = 0; i < cz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void reverse(int arr[], int cz)
{
	int left = 0;
	int right = cz - 1;
	int tmp = 0;

	while (left < right)
	{
		//tmp = arr[left];
		//arr[left]=arr[right];
		//arr[right] = tmp;
		tmp = *(arr + left);
		*(arr + left) = *(arr + right);
		*(arr + right) = tmp;
		left++;
		right--;
	}
}

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int cz = sizeof(arr) / sizeof(arr[0]);
	//init(arr, cz);
	print(arr, cz);
	reverse(arr,cz);
	//print(arr, cz);
	return 0;
}