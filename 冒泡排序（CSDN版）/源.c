#define  _CRT_SECURE_NO_WARNINGS 1
//改为升序
#include<stdio.h>
int main()
{
    int  arr[10] = { 9,5,4,3,2,0,1,6,7,8 };
    int  cz = sizeof(arr) / sizeof(arr[0]);
    int  tmp = 0;
    int i = 0, j = 0;
    int m = 1;
    int last_change = cz;
    for ( i = 0;  i < cz-1 ; i++)//大的比较
    {
        m = 1;
        for ( j = 0; (j < cz - i - 1) && ( j < last_change) ; j++)//小的比较
        {
            if (arr[j] > arr[j + 1])
            {
                tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;  
                m =0;
            }
        }
        last_change = j + 1;
        if (m)
        {
            break;
        }
    }
    for (int i = 0; i < cz; i++)
        printf("%d ", arr[i]);
    return 0;
}
