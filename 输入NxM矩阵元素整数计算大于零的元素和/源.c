#define  _CRT_SECURE_NO_WARNINGS 1

//输入NxM矩阵，矩阵元素均为整数，计算其中大于零的元素之和。
//
//输入：
//第一行为N M(N: 矩阵行数；M: 矩阵列数, 且M, N <= 10)，接下来的N行为矩阵各行。
//输出：
//一行，其中大于零的元素之和。
#include<stdio.h>

int main()
{
    unsigned int n = 10;
    unsigned int m = 10;
    scanf("%d %d", &n, &m);
    int arr[10][10] = { 0 };
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            scanf("%d ", &arr[i][j]);
            if (arr[i][j] > 0)
                sum += arr[i][j];
        }
    }
    printf("%d\n", sum);
    return 0;
}