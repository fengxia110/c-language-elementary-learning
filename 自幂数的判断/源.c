#define  _CRT_SECURE_NO_WARNINGS 1
//求出0～100000之间的所有“水仙花数”并输出。
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，
//则153是一个“水仙花数”
#include<stdio.h>
#include<math.h>
int main()
{
	for (int i = 0; i < 10001; i++)
	{
			int n = i;
			int count = 1;
			int sum = 0;
			while (n /= 10)
				count++;
			n = i;
			do
			{
				sum +=  pow( n%10 , count);
			} while (n /= 10);
			if (sum == i)
				printf("%d ", i);
		
	}
}