#define  _CRT_SECURE_NO_WARNINGS 1
//调整数组使奇数全部都位于偶数前面。
//注：使用选择排序可能更加方便

#include<stdio.h>

//int main()
//{
//	int arr[10] = { 0,1,2,3,4,5,6,7,8,9 };
//	int cz = sizeof(arr) / sizeof(arr[0]);
//	int tmp = 0;
//	for (int i = 0; i<cz-1 ; i++)
//	{
//		if (arr[i] % 2 == 0)
//		{
//			for (int j = i; j<cz-i-1; j++)
//			{
//				tmp = arr[j + 1];
//				arr[j+1] = arr[j];
//				arr[j] = tmp;
//			}
//		}
//	}
//	for (int i = 0; i < cz; i++)
//		printf("%d ", arr[i]);
//}
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int cz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = cz - 1;
	int tmp = 0;
	while (left < right)
	{
		if (arr[left] % 2 == 0)
		{
			if (arr[right] % 2 == 1)
			{
				tmp = arr[left];
				arr[left] = arr[right];
				arr[right] = tmp;
			}
			else
				right--;
		}
		else
			left++;
	}
	for (int i = 0; i < cz - 1; i++)
		printf("%d ", arr[i]);
	return 0;
}