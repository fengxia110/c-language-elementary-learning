#pragma once

//库函数的包含
//引用了输入输出函数
#include<stdio.h>
//引用随机变量
#include<stdlib.h>
//引用时间戳
#include<time.h>


#define ROW 3
#define COL 3


//函数的声明，要用分号
void init_board(char board[ROW][COL], int row, int col);
void printf_board(char board[ROW][COL], int row, int col);
void player_move(char board[ROW][COL], int row, int col);
void computer_move(char board[ROW][COL], int row, int col);
char is_win(char board[ROW][COL], int row, int col);



