#define  _CRT_SECURE_NO_WARNINGS 1

#include "game.h"

void game()
{
	//创建数组，存放变量
	char board[ROW][COL] = { 0 };
	//用于判断胜利
	char ret = '0';
	

	//初始化数组内容，和打印棋盘
	init_board(board, ROW, COL);
	printf_board(board, ROW, COL);

	//玩家下棋和电脑下棋
	do
	{
		player_move(board, ROW, COL);
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
			break;
		computer_move(board, ROW, COL);
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
			break;
	} while (1);
	//输出结果
	if (ret == '*')
	{
		printf("玩家胜\n");
	}
	else if(ret=='#')
	{
		printf("电脑胜\n");
	}
	else
	{
		printf("平局\n");
	}
}

int main()
{
	//用input来接收变量，判断是否开始游戏
	int input = 0;
	//此处设置随机值，让电脑随机下棋
	srand((unsigned int)time(NULL));
	do
	{
		//打印开始菜单
		menu();
		printf("请选择：\n");
		scanf("%d", &input);
		switch (input)
		{
		case 0:
			printf("已退出游戏！\n");
			break;
		case 1:
			printf("开始游戏\n");
			game();
			break;
		default:
			printf("输入错误，请重新输入：\n");
			break;
		}
	}
	while (input);
	
	return 0;

}
