#define  _CRT_SECURE_NO_WARNINGS 1

#include "game.h"


//打印菜单的实现
void menu()
{
	printf("******************************************\n");
	printf("************   1 . 开始游戏   ************\n");
	printf("************   0 . 退出游戏   ************\n");
	printf("******************************************\n");
}

//初始化函数的实现
void init_board(char board[ROW][COL], int row, int col)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
			board[i][j] = ' ';
	}
}

void printf_board(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{   
		//打印带有符号的行
		for (j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < col - 1)
				printf("|");
		}
		printf("\n");
		//打印分割行
		if (i < row - 1)
		{
			for (j = 0; j < col; j++)
			{
				printf("___");
				if (j < col - 1)
					printf("|");
			}
		}
		//到了最后一行为了保持对称和美观只打印了‘|’
		else
		{
			for (j = 0; j < col; j++)
			{
				printf("   ");
				if (j < col -1 )
					printf("|");
			}
			
		}
		printf("\n");
	}
}

//玩家下棋
void player_move(char board[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	
	do 
	{
		do
		{
			printf("请输入坐标：\n");
			scanf("%d %d", &x, &y);
			if (x > 0 && x <= row && y > 0 && y <= col)
				break;
			else
				printf("输入坐标错误，请重新输入：\n");
		} while (1);
		if (board[x - 1][y - 1] == ' ')
		{
			board[x - 1][y - 1] = '*';
			break;
		}
		else
		{
			printf("输入坐标已被占用，请重新输入：\n");
		}
	} while (1);
	printf_board(board, row, col);
}

//电脑随机下棋
void computer_move(char board[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	do 
	{
		x = rand() % row;
	    y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	} while (1);
	printf_board(board, row, col);
}

//判断胜负
//*代表玩家胜
//#代表电脑胜
//Q平局C继续
char is_win(char board[ROW][COL], int row, int col)
{
	int i = 0;
	//判断行胜利
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2]&&board[i][0]!=' ')
			return board[i][0];
	}
	//判断列胜利
	for (i = 0; i < col; i++)
	{
		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ')
			return board[0][i];
	}
	//判断对角线胜利
	if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ') || (board[0][2] == board[1][1] && board[1][1] == board[2][0]) && board[1][1] != ' ')
		return board[1][1];
	//判断平局或继续
	for (i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
			if (board[i][j] == ' ')
				return 'C';

	}
	return 'Q';
}