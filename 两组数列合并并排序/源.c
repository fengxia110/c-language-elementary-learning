#define  _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
    int n = 0, m = 0;
    scanf("%d %d", &n, &m);
    int arr1[n], arr2[m], arr[n + m];
    int tmp = 0;
    int key = 1;
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr1[i]);
    }
    for (int i = 0; i < m; i++)
    {
        scanf("%d", &arr2[i]);
    }
    for (int i = 0; i < n; i++)
    {
        arr[i] = arr1[i];
    }
    for (int i = 0; i < m; i++)
    {
        arr[i + n] = arr2[i];
    }
    for (int i = 0; i < n + m - 1; i++)
    {
        key = 1;
        for (int j = 0; j < n + m - 1 - i; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                key = 0;
            }
        }
        if (key == 1)
            break;
    }
    for (int i = 0; i < m + n; i++)
    {
        printf("%d ", arr[i]);
    }
}