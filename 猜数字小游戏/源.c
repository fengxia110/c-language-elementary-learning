#define  _CRT_SECURE_NO_WARNINGS 1
#define  _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<time.h>
#include<stdlib.h>

void menu()
{
    printf("**********************************\n");
    printf("*******     1.开始游戏    ********\n");
    printf("*******     0.退出游戏    ********\n");
    printf("**********************************\n");
}

void game()
{
    int answer = 0;
    int n = 0;
    answer = rand() / 100;//0~32767
    while (1)
    {
        printf("请猜测：\n");
        scanf("%d", &n);
        if (n > answer)
            printf("猜大了\n");
        else if (n < answer)
            printf("猜小了\n");
        else
        {
            printf("恭喜你获得胜利！\n");
            break;
        }

    }
}

int main()
{
    int input = 0;
    srand((unsigned)time(NULL));
    do
    {
        menu();
        scanf("%d", &input);
        switch (input)
        {
        case 1:
            printf("开始游戏\n");
            game();
            system("pause");
            system("cls");
            break;
        case 0:
            printf("已退出！\n");
            break;

        default:
            printf("输入错误，请重新输入!\n");
            break;
        }
    } while (input);
    return 0;
}


